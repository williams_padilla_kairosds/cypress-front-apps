/// <reference types="cypress" />

describe("React BackOffice post", () => {
    it("should load page", () => {
        cy.visit("https://local-apps.kairosds.com/blog/");
    });

    // unskip to test register
    it.skip("user could register", () => {
        cy.get(".bNLslF").click();
        cy.get('[placeholder="Type you email"]').type(
            "willy-author-10@hola.com"
        );
        cy.get('[placeholder="Type your password"]').type("1234");
        cy.get(".sc-crXcEl").select("Author");
        cy.get('[placeholder="Type your nickname"]').type("GhostHunter");
        cy.get(".sc-jqUVSM").click();
        cy.get(".sc-iqcoie").click();
    });

    it("user should log in", () => {
        // User is Logging
        cy.visit("/");
        cy.get(".hGSjug").click();
        cy.get('[placeholder="Type your email"]').type(
            "willy-author-10@hola.com"
        );
        cy.get('[placeholder="Type your password"]').type("1234");
        cy.get(".sc-jqUVSM").click();
        cy.get("button").should("exist");
    });

    it("user create a post, delete a post and update his post", () => {
        // User go to see, create, delete, and update his posts
        cy.get('[href="/blog/post"]').click();

        // User create his first post
        cy.get("input").type("hi i am a new user");
        cy.get("textarea").type(
            "This is my first post so please do not be mean, this post should be 300 letters long"
        );
        cy.get(".hGSjug").click();

        // User delete his first post

        cy.get(".bXofCr").click();
        cy.get(".sc-breuTD").children().should("have.length", 0);

        // User create his first post again to update

        cy.get("input").type("hi i am a new user");
        cy.get("textarea").type(
            "This is my first post so please do not be mean, this post should be 300 letters long"
        );
        cy.get(".hGSjug").click();
        // Click the update button
        cy.get(".jExbDd").click();
        cy.get(".sc-hKMtZM").should("contain", "Updating...");
        cy.get("input").clear();
        cy.get("textarea").clear();

        cy.get("input").type("I am updating my post");
        cy.get("textarea").type(
            "This is my first post so please do not be mean, this post should be 300 letters long"
        );
        cy.get(".hGSjug").click();
        cy.get(".sc-bBrHrO").should("contain", "I am updating my post");

        cy.get(".bXofCr").click();
        cy.get(".sc-breuTD").children().should("have.length", 0);
        cy.go("back");
        //
    });

    it("should load all post from other users", () => {
        cy.get('[href="/blog/public-posts"]').click();
    });

    it("user can visit comments section from a post", () => {
        cy.get(":nth-child(1) > .sc-dIouRR > .sc-fLlhyt > .sc-cxabCf").click();
        cy.wait(2000);
    });
});
