it("user should log in and create a post with a comment", () => {
    // User is Logging
    cy.visit("/");
    cy.get(".hGSjug").click();
    cy.get('[placeholder="Type your email"]').type("willy-author-10@hola.com");
    cy.get('[placeholder="Type your password"]').type("1234");
    cy.get(".sc-jqUVSM").click();
    cy.get("button").should("exist");

    //user create a post

    cy.get('[href="/blog/post"]').click();
    cy.wait(1000);
    cy.get("input").type("Title user");
    cy.get("textarea").type(
        "This is my first post so please do not be mean, this post should be 300 letters long"
    );
    cy.get(".hGSjug").click();
    cy.get(".sc-cxabCf").click();

    //user Create a comment from its own post
    cy.wait(1000);
    cy.get("input").type("This comments is going to be deleted by admin");
    cy.get("form > button").click();

    //user logout
    cy.wait(1000);
    cy.get(".sc-jSMfEi > button").click();
});

it("should appear a post created with its comment", () => {
    cy.visit("https://local-apps.kairosds.com/front-app/");
    cy.get(":nth-child(1) > .post__info > .comment_button").click();
    cy.get(".comment_detail__container").should("exist");
    cy.wait(2000);
});

it("admin should log in and delete a comment", () => {
    cy.visit("/");
    cy.get(".hGSjug").click();
    cy.get('[placeholder="Type your email"]').type("willy-admin@hola.com");
    cy.get('[placeholder="Type your password"]').type("1234");
    cy.get(".sc-jqUVSM").click();
    cy.get("button").should("exist");

    //admin check posts
    cy.get('[href="/blog/post"]').click();
    cy.wait(1000);
    cy.get(":nth-child(1) > .sc-dIouRR > .sc-fLlhyt > .sc-cxabCf").click();

    // //deleting the comment
    cy.wait(1000);
    cy.get("span > :nth-child(2)").click();

    //visiting public page and see the comment is not there anymore

    cy.visit("https://local-apps.kairosds.com/front-app/");
    cy.get(":nth-child(1) > .post__info > .comment_button").click();
    cy.wait(1000);
    cy.get(".comment_detail__container").should("not.exist");

    //going back to back office app and delete the user
});

it("admin should log in and delete a post", () => {
    cy.visit("/");
    cy.get(".hGSjug").click();
    cy.get('[placeholder="Type your email"]').type("willy-admin@hola.com");
    cy.get('[placeholder="Type your password"]').type("1234");
    cy.get(".sc-jqUVSM").click();
    cy.get("button").should("exist");

    //admin check posts
    cy.get('[href="/blog/post"]').click();
    cy.wait(1000);

    //deleting the post
    cy.wait(1000);
    cy.get(":nth-child(1) > .sc-hAZoDl > .bXofCr").click();

    //visiting the public page
    cy.wait(1000);
    cy.visit("https://local-apps.kairosds.com/front-app/");
    cy.get("div.post__container").children().should("have.length", 2);
});
