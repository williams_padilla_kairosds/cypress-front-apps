// -------------------> Crud comments <-----------------------//
it("user should log in and go to public post", () => {
    // User is Logging
    cy.visit("/");
    cy.get(".hGSjug").click();
    cy.get('[placeholder="Type your email"]').type("willy-author-10@hola.com");
    cy.get('[placeholder="Type your password"]').type("1234");
    cy.get(".sc-jqUVSM").click();
    cy.get("button").should("exist");
});
it("user can create, delete and update his own comments", () => {
    cy.get('[href="/blog/public-posts"]').click();
    cy.get(":nth-child(1) > .sc-dIouRR > .sc-fLlhyt > .sc-cxabCf").click();
    cy.wait(1000);

    cy.get("input").type("Im just a comment test");
    cy.get("form > button").click();

    //deleting comment
    cy.wait(1000);
    cy.get(".sc-llJcti > div > :nth-child(2) > :nth-child(2)").click();

    //creating a comment to be updated
    cy.get("input").type("Im just a comment test");
    cy.get("form > button").click();

    cy.get(".sc-llJcti > div > :nth-child(2) > :nth-child(3)").click();
    cy.get("input").clear().type("Im just a comment updated");
    cy.get("form > button").click();

    //deleting comment
    cy.wait(1000);
    cy.get(".sc-llJcti > div > :nth-child(2) > :nth-child(2)").click();
});

// it("user can  update his own comment", () => {});
// it("user can create comment", () => {
//     // login("willy-author-10@hola.com");
// });
