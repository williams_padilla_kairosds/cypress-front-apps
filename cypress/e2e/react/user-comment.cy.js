beforeEach(() => {
    cy.visit("/");
    cy.get(".hGSjug").click();
    cy.get('[placeholder="Type your email"]').type("willy-user@hola.com");
    cy.get('[placeholder="Type your password"]').type("1234");
    cy.get(".sc-jqUVSM").click();
    cy.get("button").should("exist");
});

it("user can only comment in existing posts and delete", () => {
    cy.wait(1000);
    cy.get('[href="/blog/public-posts"]').click();

    cy.wait(1000);
    cy.get(":nth-child(1) > .sc-dIouRR > .sc-fLlhyt > .sc-cxabCf").click();
    cy.get("input").type(
        "This is my first post so please do not be mean, this post should be 300 letters long"
    );
    cy.get("form > button").click();

    //deleting comment
    cy.wait(1000);
    cy.get("div > :nth-child(3) > :nth-child(2)").click();
});

it("user can update his own comment", () => {
    cy.wait(1000);
    cy.get('[href="/blog/public-posts"]').click();

    cy.wait(1000);
    cy.get(":nth-child(2) > .sc-dIouRR > .sc-fLlhyt > .sc-cxabCf").click();

    cy.get("input").type(
        "This is my first post so please do not be mean, this post should be 300 letters long"
    );
    cy.get("form > button").click();

    //updating comment
    cy.wait(1000);
    cy.get("span > :nth-child(3)").click();

    cy.get("input").clear().type(" this post should be 300 letters long");
    cy.get("form > button").click();

    //deleting comment
    cy.wait(1000);
    cy.get("span > :nth-child(2)").click();
});
