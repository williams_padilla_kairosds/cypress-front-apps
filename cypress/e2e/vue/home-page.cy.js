it("user can visit home page", () => {
    cy.visit("https://local-apps.kairosds.com/front-app/");
});

it("user can visit section comment", () => {
    cy.get(":nth-child(1) > .post__info > .comment_button").click();
});
